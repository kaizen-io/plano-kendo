window.onload = function() {
    const MenuOpenButton = document.querySelector('.menu');
    const MenuCloseButton = document.querySelector('.menu-exit');
    const MobileMenu = document.querySelector('.mobile-menu'); 
    const TopBar = document.querySelector("nav");
    const currentPage = window.location.pathname;
    
    MobileMenu.addEventListener("transitionend", () => {
        MobileMenu.style.visibility = "visible";
    });

    let scrollBefore = 0;
    let deltaY = 0;

    window.addEventListener('scroll', () => {
        deltaY = window.scrollY - scrollBefore;
        scrollBefore = window.scrollY;
        
        if (deltaY < 0 && window.scrollY >= window.innerHeight) {
            TopBar.classList.add("hide");
        } else if (deltaY < -20 && window.scrollY <= window.innerHeight / 4) {
            TopBar.classList.remove("animate");
            TopBar.classList.remove("hide");
        } else {
            TopBar.classList.add("animate");
            TopBar.classList.remove("hide");
        }
        
        if ((currentPage === "/index.html" || currentPage === "/") && window.innerWidth <= 1460) {
            if (window.scrollY <= window.innerHeight / 3) {
                TopBar.classList.add("index");
            } else {
                TopBar.classList.remove("index");
            }
        }
    })
    
    function scroll(element) {
        MobileMenu.classList.remove("menu-active");  
        
        if ('scrollBehavior' in document.documentElement.style) {
            window.scrollTo({
                top: window.scrollY + document.getElementById(element).getBoundingClientRect().top,
                left: window.scrollX + document.getElementById(element).getBoundingClientRect().left,
                behavior: "smooth",
            })
        } else {
            window.scrollTo(
                window.pageXOffset + document.getElementById(element).getBoundingClientRect().left,
                window.pageYOffset + document.getElementById(element).getBoundingClientRect().top
            );
        }
    }

    if (currentPage === "/index.html" || currentPage === "/") {
        const HomeLink = [...document.querySelectorAll("#home-link")];
        HomeLink.forEach((e) => {
            e.addEventListener("click", () => {
                scroll("home");
            });
        })
    }
    
    if (currentPage === "/contact/") {
        const HoursLink = [...document.querySelectorAll("#hours-link")];
        HoursLink.forEach((e) => {
            e.addEventListener("click", () => {
                scroll("hours");
            });
        })

        const LocationLink = [...document.querySelectorAll("#location-link")];
        LocationLink.forEach((e) => {
            e.addEventListener("click", () => {
                scroll("location");
            });
        })
    }
    
    if (currentPage === "/dojo/") {
        const SenseiArticle = document.querySelector(".words-block article");
        const Headers = SenseiArticle.querySelector(".headers");
        const LanguageButtons = [...SenseiArticle.querySelectorAll("button")];
        const Message = SenseiArticle.querySelector('.sensei-message');
        let lastSelected = 0;

        LanguageButtons[0].addEventListener("click", () => {
            Message.children[lastSelected].classList.toggle("hide");
            Headers.children[lastSelected].classList.toggle("hide");
            LanguageButtons[lastSelected].classList.toggle("active");
            Message.children[0].classList.toggle("hide");
            Headers.children[0].classList.toggle("hide");
            LanguageButtons[0].classList.toggle("active");

            lastSelected = 0;
        });
        LanguageButtons[1].addEventListener("click", () => {
            Message.children[lastSelected].classList.toggle("hide");
            Headers.children[lastSelected].classList.toggle("hide");
            LanguageButtons[lastSelected].classList.toggle("active");
            Message.children[1].classList.toggle("hide");
            Headers.children[1].classList.toggle("hide");
            LanguageButtons[1].classList.toggle("active");

            lastSelected = 1;
        });
        LanguageButtons[2].addEventListener("click", () => {
            Message.children[lastSelected].classList.toggle("hide");
            Headers.children[lastSelected].classList.toggle("hide");
            LanguageButtons[lastSelected].classList.toggle("active");
            Message.children[2].classList.toggle("hide");
            Headers.children[2].classList.toggle("hide");
            LanguageButtons[2].classList.toggle("active");

            lastSelected = 2;
        });
    }
    
    MenuOpenButton.addEventListener("click", () => {
        MobileMenu.classList.add("menu-active");   
    });

    MenuCloseButton.addEventListener("click", () => {
        MobileMenu.classList.remove("menu-active");
    })
};
